import pdfkit
from django.core.files import File
import io
import qrcode
import base64


def get_pdf(template):
    pdf = pdfkit.from_string(template)
    return File(io.BytesIO(pdf), name='file.pdf')


def get_qrcode(url):
    img = qrcode.make(url)
    img_bytes_array = io.BytesIO()
    img.save(img_bytes_array, format='PNG')
    encoded_image = base64.b64encode(img_bytes_array.getvalue())
    return encoded_image.decode('utf-8')
