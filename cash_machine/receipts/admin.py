from django.contrib import admin
from receipts.models import Receipt, ReceiptItem

admin.site.register(Receipt)
admin.site.register(ReceiptItem)
