from django.urls import path, re_path
from receipts.views import CreateReceipt, download


urlpatterns = [
    path('cash_machine/', CreateReceipt.as_view(), name='cash-machine'),
    re_path(r'media/(?P<filename>[0-9a-z]+\.pdf)$', download, name='download')
]
