from drf_yasg.openapi import Schema, Parameter
import drf_yasg.openapi as openapi


items = Schema(
    title='Items',
    type=openapi.TYPE_ARRAY,
    items=openapi.Items(type=openapi.TYPE_INTEGER)
)

items_object = Schema(
    title='Items object',
    type=openapi.TYPE_OBJECT,
    properties={
        'items': items
    },
    required=['items']
)
