from django.db import models
from products.models import Item
import uuid


def pdf_path(instance, filename):
    return '{}.pdf'.format(uuid.uuid4().hex)


class Receipt(models.Model):
    items = models.ManyToManyField(Item, through='ReceiptItem')
    file = models.FileField(upload_to=pdf_path, blank=True)

    def get_items(self):
        return self.items.all()

    def total_price(self):
        sum = 0
        for item in self.get_items():
            sum += item.receiptitem_set.filter(receipt=self)[0].total_price()
        return sum

    def receipt_items(self):
        items = [(item.title, item.receiptitem_set.filter(receipt=self)[0].count,
                  str(item.receiptitem_set.filter(receipt=self)[0].total_price()))
                 for item in self.get_items()]
        return {'items': items}


class ReceiptItem(models.Model):
    item = models.ForeignKey(Item, on_delete=models.SET_NULL, null=True)
    receipt = models.ForeignKey(Receipt, on_delete=models.SET_NULL, null=True)
    count = models.IntegerField(default=1)

    def total_price(self):
        return self.item.price * self.count if self.item != None else 0
