from django.shortcuts import render
from rest_framework import views
from rest_framework.response import Response
from rest_framework import status
from django.template.loader import render_to_string
from django.http import HttpResponse
from receipts.models import Receipt
from utils.utils import get_pdf, get_qrcode
from receipts.serializers import ReceiptSerializer
from django.conf import settings
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.reverse import reverse
from receipts.renderers import BinaryFileRenderer
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from drf_yasg.inspectors import SwaggerAutoSchema
from receipts.openapi_properties_responses import items_object
from datetime import datetime


filename_param = openapi.Parameter(
    'filename', openapi.IN_QUERY, type=openapi.TYPE_STRING)
pdf_file_response = openapi.Response('Pdf file', type=openapi.TYPE_FILE)
html_file_response = openapi.Response(
    'HTML file with qrcode', type=openapi.TYPE_FILE)


@swagger_auto_schema(method='GET', auto_schema=None)
@api_view(['GET'])
def api_root(request):
    return Response({
        'cash': reverse('cash-machine', request=request),
        'download': reverse('download', request=request,
                            kwargs={'filename': '4c03ef032f4346c1b032fcaf545cc41e.pdf'}),
        'items': reverse('item-list', request=request)
    })


@swagger_auto_schema(method='GET',
                     manual_parameters=[filename_param],
                     responses={
                         '200': pdf_file_response,
                         '404': openapi.TYPE_STRING
                     })
@api_view()
@renderer_classes([BinaryFileRenderer,])
def download(request, filename=None):
    try:
        with open(settings.MEDIA_ROOT / filename, 'rb') as report:
            return Response(
                report.read(),
                headers={'Content-Disposition': 'attachment; filename="file.pdf"'},
                content_type='application/pdf')
    except Exception as e:
        return Response(status=status.HTTP_404_NOT_FOUND)


class CreateReceipt(views.APIView):
    queryset = Receipt.objects.all()
    serializer_class = ReceiptSerializer

    class TextHtmlAutoSchema(SwaggerAutoSchema):
        def get_produces(self):
            return ["text/html"]

        def get_consumes(self):
            return ["text/html"]

    @swagger_auto_schema(auto_schema=TextHtmlAutoSchema, request_body=items_object, responses={
        '200': html_file_response,
        '404': openapi.TYPE_STRING
    })
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            receipt = serializer.save()
            receipt_items = receipt.receipt_items()
            receipt_items['total'] = receipt.total_price()
            now = datetime.now()
            receipt_items['datetime'] = now.strftime("%d.%m.%Y, %H:%M")
            pdf_template = render_to_string(
                'receipt.jinja', context=receipt_items, request=request)
            receipt.file = get_pdf(pdf_template)
            receipt.save()
            url = '{}://{}:{}'.format(request.scheme,
                                      request.META['HTTP_HOST'], receipt.file.url)
            qrcode = 'data:image/png;base64, {}'.format(get_qrcode(url))
            response_template = render_to_string('qrcode.jinja',
                                                 context={'qrcode': qrcode},
                                                 request=request)
            return HttpResponse(response_template)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
