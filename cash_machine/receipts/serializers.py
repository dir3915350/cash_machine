from rest_framework import serializers
from receipts.models import Receipt
from products.models import Item


class ReceiptSerializer(serializers.ModelSerializer):
    items = serializers.PrimaryKeyRelatedField(
        many=True, queryset=Item.objects.all())

    class Meta:
        model = Receipt
        fields = ['items']
