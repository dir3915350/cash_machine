from rest_framework import routers
from products.views import ItemViewSet


router = routers.SimpleRouter()
router.register(r'items', ItemViewSet)
