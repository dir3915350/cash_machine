from django.shortcuts import render
from rest_framework import viewsets, mixins
from products.models import Item
from products.serializers import ItemSerializer
from django.utils.decorators import method_decorator
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from products.serializers import ItemSerializer


@method_decorator(name='create', decorator=swagger_auto_schema(
    request_body=ItemSerializer,
    responses={
        '201': ItemSerializer,
        '400': openapi.TYPE_STRING
    }
))
class ItemViewSet(viewsets.GenericViewSet,
                  mixins.CreateModelMixin,
                  mixins.ListModelMixin):
    queryset = Item.objects.all()
    serializer_class = ItemSerializer
