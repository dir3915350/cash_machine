### Заметки

[Узнать ip компьютера в windows](https://www.mercusys.com/ru/faq-304)

[Узнать ip компьютера в linux](https://pingvinus.ru/note/determine-ip-address#:~:text=%D0%A7%D1%82%D0%BE%D0%B1%D1%8B%20%D0%BE%D0%BF%D1%80%D0%B5%D0%B4%D0%B5%D0%BB%D0%B8%D1%82%D1%8C%20IP%2D%D0%B0%D0%B4%D1%80%D0%B5%D1%81%20%D0%B2%D0%B0%D1%88%D0%B5%D0%B3%D0%BE,addr%20%D0%B8%D0%BB%D0%B8%20%D0%BF%D1%80%D0%BE%D1%81%D1%82%D0%BE%20ip%20a%29.&text=%D0%92%20%D1%80%D0%B5%D0%B7%D1%83%D0%BB%D1%8C%D1%82%D0%B0%D1%82%D0%B5%20%D0%B2%D1%8B%D0%BF%D0%BE%D0%BB%D0%BD%D0%B5%D0%BD%D0%B8%D1%8F%20%D0%BA%D0%BE%D0%BC%D0%B0%D0%BD%D0%B4%D1%8B%20%D0%BD%D0%B0,%D1%81%D0%B5%D1%82%D0%B5%D0%B2%D1%8B%D1%85%20%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D1%84%D0%B5%D0%B9%D1%81%D0%BE%D0%B2%20%D0%B8%20%D0%B8%D1%85%20%D0%BF%D0%B0%D1%80%D0%B0%D0%BC%D0%B5%D1%82%D1%80%D1%8B.)

Установить пакеты необходимые для работы [pdfkit](https://pypi.org/project/pdfkit/)

### Запуск

Выполнить следующую последовательность комманд(при разработке использовался python3.8).

```
python3 -m venv cash_machine
```

```
cd cash_machine
```

```
source bin/activate
```

```
git clone https://gitlab.com/dir3915350/cash_machine.git
```
```
cd cash_machine
```

```
pip install -r requirements.txt
```

```
cd cash_machine
```

```
./manage.py runserver 0.0.0.0:8000
```

### Выполнение запросов к api 

http&#65279;://<ip адрес вашего компьютера>:8000/docs - SwaggerUI

http&#65279;://<ip адрес вашего компьютера>:8000/api-root - Browsable API

